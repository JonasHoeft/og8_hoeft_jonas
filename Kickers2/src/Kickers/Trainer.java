package Kickers;

public class Trainer extends Person{
private String lizenzKlasse;
private double aufwandEntschaedigung;
Manschaft manschaft;


public Trainer(String name, String telefonnummer,String lizenzKlasse, double aufwandEntschaedigung,Manschaft manschaft) {
	super(name, telefonnummer);
	this.lizenzKlasse = lizenzKlasse;
	this.aufwandEntschaedigung = aufwandEntschaedigung;
	this.manschaft = manschaft;
}

public String getLizenzKlasse() {
	return lizenzKlasse;
}

public void setLizenzKlasse(String lizenzKlasse) {
	this.lizenzKlasse = lizenzKlasse;
}
public double getAufwandEntschaedigung() {
	return aufwandEntschaedigung;
}
public void setAufwandEntschaedigung(double aufwandEntschaedigung) {
	this.aufwandEntschaedigung = aufwandEntschaedigung;
}

@Override
public String toString() {
	return "Trainer [lizenzKlasse=" + lizenzKlasse + ", aufwandEntschaedigung=" + aufwandEntschaedigung + ", manschaft="
			+ manschaft + "]";
}

}
