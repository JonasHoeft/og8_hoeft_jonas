package key;

import java.util.Arrays;

public class KeyStore01 {
	String[] array = new String[100];

	public KeyStore01() {

	}

	public boolean add(String eintrag) {
		for (int i = 0; i < array.length; i++) {
			if (array[i] == null) {
				array[i] = eintrag;
				return true;
			}
		}
		return false;
	}

	public int indexOf(String eintrag) {
		for (int i = 0; i < array.length; i++) {
			if(array[i].equals(null)) return -1;
			if (array[i].equals(eintrag))return i;
		}
		return -1;
	}

	public boolean remove(int index) {
		if(index > (array.length - 1))return false;
			array[index] = null;
		return true;
	}

	@Override
	public String toString() {
		return "KeyStore01 [array=" + Arrays.toString(array) + "]";
	}

}
