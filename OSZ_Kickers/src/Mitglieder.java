
public class Mitglieder {

	
//Attribute
private String name;
private int Telefonnummer;
private boolean jahresbeitrag;

public Mitglieder(String name, int telefonnummer, boolean jahresbeitrag) {
	super();
	this.name = name;
	this.Telefonnummer = telefonnummer;
	this.jahresbeitrag = jahresbeitrag;
}

public boolean isJahresbeitrag() {
	return jahresbeitrag;
}

public void setJahresbeitrag(boolean jahresbeitrag) {
	this.jahresbeitrag = jahresbeitrag;
}

public Mitglieder() {
	super();
}


public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}
public int getTelefonnummer() {
	return Telefonnummer;
}
public void setTelefonnummer(int telefonnummer) {
	Telefonnummer = telefonnummer;
}
	
}
