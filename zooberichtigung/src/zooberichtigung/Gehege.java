package zooberichtigung;

public class Gehege {
private double flaeche;
private int preis;
private String attraktivitaet;
private Affe[] ListAffen;



public Affe[] getListAffen() {
	return ListAffen;
}
public void setListAffen(Affe[] listAffen) {
	ListAffen = listAffen;
}
public Gehege() {
	super();
}
public double getFlaeche() {
	return flaeche;
}
public void setFlaeche(double flaeche) {
	this.flaeche = flaeche;
}
public int getPreis() {
	return preis;
}
public void setPreis(int preis) {
	this.preis = preis;
}
public String getAttraktivitaet() {
	return attraktivitaet;
}
public void setAttraktivitaet(String attraktivitaet) {
	this.attraktivitaet = attraktivitaet;
} 




}
