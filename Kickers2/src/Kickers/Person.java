package Kickers;

public class Person {

	private String name;
	private String telefonnummer;

public Person() {
	super();
}


public Person(String name, String telefonnummer) {
	super();
	this.name = name;
	this.telefonnummer = telefonnummer;
}


public String getTelefonnummer() {
	return telefonnummer;
}


public void setTelefonnummer(String telefonnummer) {
	this.telefonnummer = telefonnummer;
}


public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}

@Override
public String toString() {
	return "Person [name=" + name + ", telefonnummer=" + telefonnummer + "]";
}


}
