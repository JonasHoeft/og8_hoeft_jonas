package Kickers;

public class Schiedsrichter extends Person {
	private int anzSpiele;

	public Schiedsrichter(int anzSpiele) {
		super();
		this.anzSpiele = anzSpiele;
	}

	public int getAnzSpiele() {
		return anzSpiele;
	}

	public void setAnzSpiele(int anzSpiele) {
		this.anzSpiele = anzSpiele;
	}

}
