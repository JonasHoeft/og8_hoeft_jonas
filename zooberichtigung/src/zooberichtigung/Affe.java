package zooberichtigung;

public class Affe {
private int alter;
private String name;
Gehege gehege;

public Affe(String name) {
	super();
	this.name = name;
}
public int getAlter() {
	return alter;
}
public void setAlter(int alter) {
	this.alter = alter;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public Gehege getGehege() {
	return gehege;
}
public void setGehege(Gehege gehege) {
	this.gehege = gehege;
}

}
