/* Fibonacci.java
   Programm zum Testen der rekursiven und der iterativen Funktion
   zum Berechnen der Fibonacci-Zahlen.
   AUFGABE: Implementieren Sie die Methoden fiboRekursiv() und fiboIterativ()
   HINWEIS: siehe Informationsblatt "Fibonacci-Zahlen oder das Kaninchenproblem"
   Autor:
   Version: 1.0
   Datum:
*/
public class Fibonacci{
   // Konstruktor
   Fibonacci(){
   }
   
  /**
    * Rekursive Berechnung der Fibonacci-Zahl an n-te Stelle
    * @param n 
    * @return die n-te Fibonacci-Zahl
   */
 public long fiboRekursiv(int n){
	 
		    if (n <= 2) { 
		        return 1 ; 
		    } 
		    return fiboRekursiv(n - 1) + fiboRekursiv(n - 2); 
		}
	  
	  
	  //vorl�ufig:
    
  //fiboRekursiv

  /**
   * Iterative Berechnung der Fibonacci-Zahl an n-te Stelle
   * @param n 
   * @return die n-te Fibonacci-Zahl
  */
  public long fiboIterativ(int n){
    if(n <= 2) {
	  return 1;
    }
	  long f1 = 0;
	  long f2 = 1;
	  for(long i = 1;i < n; i++) {
		long f3 = f1 + f2;
		f1 = f2;
		f2 = f3;
	  }
  return f2;
  }//fiboIterativ

}// Fibonnaci


