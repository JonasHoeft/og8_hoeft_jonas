
public class Spieler extends Mitglieder {

	private int Trickonummer;
	private String Spielposition;
	
	public Spieler(String name, int telefonnummer) {
		super(name, telefonnummer);
	}
	public Spieler(String name, int telefonnummer, int trickonummer, String spielposition) {
		super(name, telefonnummer);
		Trickonummer = trickonummer;
		Spielposition = spielposition;
	}
	public int getTrickonummer() {
		return Trickonummer;
	}
	public void setTrickonummer(int trickonummer) {
		Trickonummer = trickonummer;
	}
	public String getSpielposition() {
		return Spielposition;
	}
	public void setSpielposition(String spielposition) {
		Spielposition = spielposition;
	}
	




}
