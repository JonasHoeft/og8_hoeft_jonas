package Kickers;

public class Manschaftsleiter extends Spieler{
	private String manschaft;
private int rabat;





public Manschaftsleiter(String name, String telefonnummer, int trikonummer, String position, Manschaft manschaft,
		String manschaft2, int rabat) {
	super(name, telefonnummer, trikonummer, position, manschaft);
	this.rabat = rabat;
}
public void setManschaft(String manschaft) {
	this.manschaft = manschaft;
}
public int getRabat() {
	return rabat;
}
public void setRabat(int rabat) {
	this.rabat = rabat;
}
@Override
public String toString() {
	return "Manschaftsleiter [manschaft=" + manschaft + ", rabat=" + rabat + "]";
}

}
