package Kickers;

public class Manschaft {
private String name;
private String spielklasse;
private int minSpielern = 11;
private int maxSpieler = 22;
public String getName() {
	return name;
}

public Manschaft(String name, String spielklasse) {
	super();
	this.name = name;
	this.spielklasse = spielklasse;
}

public void setName(String name) {
	this.name = name;
}
public String getSpielklasse() {
	return spielklasse;
}
public void setSpielklasse(String spielklasse) {
	this.spielklasse = spielklasse;
}
public int getMinSpielern() {
	return minSpielern;
}
public int getMaxSpieler() {
	return maxSpieler;
}

@Override
public String toString() {
	return "Manschaft [name=" + name + ", spielklasse=" + spielklasse ;
}


}
