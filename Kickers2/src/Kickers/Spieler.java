package Kickers;

public class Spieler extends Person{

	private int trikonummer;
	private String position;
	Manschaft manschaft;
	
	
	public Spieler(String name, String telefonnummer,int trikonummer, String position, Manschaft manschaft) {
		super(name, telefonnummer);
		this.trikonummer = trikonummer;
		this.position = position;
		this.manschaft = manschaft;
	}
public Manschaft getManschaft() {
	return manschaft;
}
public void setManschaft(Manschaft manschaft) {
	this.manschaft = manschaft;
}
public int getTrikonummer() {
	return trikonummer;
}
public void setTrikonummer(int trikonummer) {
	this.trikonummer = trikonummer;
}
public String getPosition() {
	return position;
}
public void setPosition(String position) {
	this.position = position;
}
@Override
public String toString() {
	return "Spieler [trikonummer=" + trikonummer + ", position=" + position + ", manschaft=" + manschaft + "]";
}


}
