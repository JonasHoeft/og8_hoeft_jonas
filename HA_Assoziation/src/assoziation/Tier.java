package assoziation;
import java.util.ArrayList;
public class Tier {

	private String name;
	private int  alter;

	
	public Tier() {
		super();
	}
	public Tier(String art, int alter) {
		this.name = art;
		this.alter = alter;
	}
	public String getArt() {
		return name;
	}
	public void setArt(String art) {
		this.name = art;
	}
	public int getAlter() {
		return alter;
	}
	public void setAlter(int alter) {
		this.alter = alter;
	}

}
